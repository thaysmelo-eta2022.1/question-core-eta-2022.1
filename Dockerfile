# imagem do repositorio.
FROM registry.gitlab.com/thaysmeloeta20221/question-core-eta20221:dependencies

COPY . .

RUN mkdir -p assets/static \
  && python manage.py collectstatic --noinput
  
CMD ["python","-u", "production-server.py"]
